const inputEl = document.querySelector("#input");
const outputEl = document.querySelector("#output");


inputEl.addEventListener("change", (ev) => {
  const {value} = ev.target;

  const result = value
    .split("\n")
    .map(el => {
      return `"${el.replace(/"/g, '\\"')}"+`;
    });


  outputEl.value = result.join("\n")
});